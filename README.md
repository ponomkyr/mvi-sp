## Conditional Deep Convolutional Generative Adversarial Networks for Multirational Faces Generation

Generative Adversarial Networks (GANs) are relatively new and powerful generative AI techniques
that allow us to generate images, text or other media based on the training samples that were fed to
a model. There exist many modifications of GAN
models depending on the specific task that needs to
be solved.

In this project, we will discuss implementation of
our own Conditional Deep Convolutional Generative Adversarial Network (cDCGAN) to generate
images of human faces based on a person’s race.
Papers, where Conditional Generative Adversarial Networks (CGAN) [2] and Deep Convolutional
Generative Adversarial Networks (DCGAN) [3] had
been shown to the world, were used as a reference
for our model.

The dataset can be downloaded here: https://drive.google.com/file/d/1JiLAekfpsHtFLhieqo9w1Oq_y9gpYafu/view?usp=sharing

The notebook on GoogleColab can be accessed here: https://colab.research.google.com/drive/1vWfL85mEefLas6lB3Y5p2vYIFfoDUEj7?usp=sharing
